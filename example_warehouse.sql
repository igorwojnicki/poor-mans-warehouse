BEGIN;

CREATE TABLE time(
       time_id INTEGER PRIMARY KEY,
       day INTEGER,
       month INTEGER,
       year INTEGER,
       UNIQUE (day, month, year)
       );


CREATE TABLE location(
       location_id INTEGER PRIMARY KEY,
       zip VARCHAR,
       city VARCHAR,
       country VARCHAR,
       UNIQUE (zip, city, country)
       );
       
CREATE TABLE sales(
       time_id INTEGER REFERENCES time,
       location_id INTEGER REFERENCES location,
       revenue NUMERIC(10,2),
       quantity INTEGER,
       PRIMARY KEY (time_id, location_id)
);     

INSERT INTO time VALUES (1,   1, 1,2020),
       	    	 	(2,   2, 1,2020),
       	    	 	(3,   3, 1,2020),
       	    	 	(4,   4, 1,2020),
       	    	 	(5,   5, 1,2020),
       	    	 	(6,   6, 1,2020),
       	    	 	(7,   7, 1,2020),
       	    	 	(8,   8, 1,2020),
       	    	 	(9,   9, 1,2020),
			(11,  1, 2,2020),
       	    	 	(12,  2, 2,2020),
       	    	 	(13,  3, 2,2020),
       	    	 	(14,  4, 2,2020),
       	    	 	(15,  5, 2,2020),
       	    	 	(16,  6, 2,2020),
       	    	 	(17,  7, 2,2020),
       	    	 	(18,  8, 2,2020),
       	    	 	(19,  9, 2,2020),
			(21,  1, 3,2020),
       	    	 	(22,  2, 3,2020),
       	    	 	(23,  3, 3,2020),
       	    	 	(24,  4, 3,2020),
       	    	 	(25,  5, 3,2020),
       	    	 	(26,  6, 3,2020),
       	    	 	(27,  7, 3,2020),
       	    	 	(28,  8, 3,2020),
       	    	 	(29,  9, 3,2020);

INSERT INTO location VALUES (1, '31341' , 'St.Louis', 'USA'),
       	    	     	    (2, '30059' , 'Kraków', 'Poland'),
       	    	     	    (3, '30340' , 'Kraków', 'Poland'),
       	    	     	    (4, '00001' , 'Warszawa', 'Poland');

INSERT INTO sales VALUES (1, 1, 30, 5),
       	    	  	 (1, 2, 60, 15),
       	    	  	 (1, 3, 90, 15),
       	    	  	 (1, 4, 20, 2),
       	    	  	 (2, 1, 60, 8),
       	    	  	 (2, 2, 90, 3),
       	    	  	 (2, 3, 40, 5),
       	    	  	 (5, 1, 70, 5),
       	    	  	 (7, 1, 30, 3),
       	    	  	 (7, 2, 80, 25),
       	    	  	 (28, 1, 80, 5),
       	    	  	 (29, 1, 90, 5);





COMMIT;
